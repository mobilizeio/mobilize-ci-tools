FROM ubuntu:20.04

# Replace shell with bash so we can source files
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    apt-transport-https \
    build-essential \
    ca-certificates \
    curl \
    git \
    libssl-dev \
    wget \
    unzip \
    python3-pip \
    libmagic-dev \
    libpq-dev \
    libc6-dev \
    gcc \
    ruby-dev \
    patch \
    zlib1g-dev \
    liblzma-dev \
    libgmp-dev \
    imagemagick \
    && rm -rf /var/lib/apt/lists/*

####
#### BEGIN NODE ####
####

ENV NVM_DIR /usr/local/nvm
ENV NODE_VERSION 11.3.0

WORKDIR $NVM_DIR

RUN curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash \
    && . $NVM_DIR/nvm.sh \
    && nvm install $NODE_VERSION \
    && nvm alias default $NODE_VERSION \
    && nvm use default

ENV NODE_PATH $NVM_DIR/versions/node/v$NODE_VERSION/lib/node_modules
ENV PATH      $NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH

#### END NODE ####

####
#### BEGIN RUBY ####
####

ENV RVM_DIR /usr/local/rvm
ENV RUBY_VERSION 2.7.0

WORKDIR $RVM_DIR

RUN gpg \
    --keyserver hkp://pool.sks-keyservers.net \
    --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB

RUN curl -sSL https://get.rvm.io | bash

ENV PATH    $RVM_DIR/bin:$PATH

# Install Ruby
RUN rvm install $RUBY_VERSION \
    && rvm use --default 2.7.0
ENV PATH    $RVM_DIR/rubies/ruby-$RUBY_VERSION/bin:$PATH

#### END RUBY ####

####
#### BEGIN AWS-CLI ####
####

ENV AWS_CLI_DIR /usr/local/aws-cli

WORKDIR $AWS_CLI_DIR

# https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-linux.html#cliv2-linux-install
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" \
    && unzip awscliv2.zip \
    && ./aws/install

#### END NODE ####

####
#### BEGIN GOOGLE CHROME ####
####

ENV CHROME_DIR /usr/local/chrome

WORKDIR $CHROME_DIR

RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
    && echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list
# Install Chrome.
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -y install \
    google-chrome-stable \
    && rm -rf /var/lib/apt/lists/*

#### END GOOGLE CHROME ####


RUN useradd -ms /bin/bash circleci
USER circleci
WORKDIR /usr/src/app
